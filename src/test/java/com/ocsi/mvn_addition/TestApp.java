package com.ocsi.mvn_addition;

import java.util.Random;
import junit.framework.TestCase;

public class TestApp extends TestCase{

    public void testAddition() {
        Addition add = new Addition();
        Random rand = new Random();
        int a;
        int b;
        for (int i= 0 ;  i < 10000 ; i++) {
        	a = rand.nextInt(5000);
        	b = rand.nextInt(5000);
        	
            add.setNombre1(a);
            add.setNombre2(b);
            assertEquals(add.getResultatAddition(), (a+b));
        }    
    }
    
    
    public void testMultiplication() {
    	Multiplication mult = new Multiplication();
        Random rand = new Random();
        int a;
        int b;
        for (int i= 0 ;  i < 10000 ; i++) {
        	a = rand.nextInt(5000);
        	b = rand.nextInt(5000);
        	
        	mult.setNombre1(a);
        	mult.setNombre2(b);
            assertEquals(mult.getResultatMultiplication(), (a*b));
        }  
    }
    
}
